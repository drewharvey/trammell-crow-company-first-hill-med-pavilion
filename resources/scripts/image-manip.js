var CacheImages = function(pageTable) {
    var numImages = 0;
    var numLoaded = 0;
    var pageContents = [];
    var cachedImages = [];
    
    // get all page contents and total num images to load
    var page = "";
    for (key in PageTable) {
        if (pageTable[key].path != "" || pageTable[key].path != undefined) {
            page = GetPageContents(PageTable[key].path);
            pageContents.push(page);
            numImages += $(page).find("img").length;
        }
    }
    
    // go thru every page and load the images
    page = ""; var progress = 0;
    for (key in pageContents) {
        page = pageContents[key];
        $(page).find("img").each(function() {
            
            // cache image (preload)
            var img = new Image();
            img.src = $(this).attr("src");
            cachedImages.push(img); 
            numLoaded++;
            
            // update progress bar
            progress = (numLoaded / numImages) * 100;
            UpdateProgressBar(progress);
        });
    }   
}

var ScaleToFit = function ($content, $wrapper) {

    var dimension = {
        width: 0,
        height: 0,
        scale: 0
    }
    
    if ($content.length < 1 || $wrapper.length < 1)
        return;
    
    var scale = 1.0;
    var wrapperHeight = $wrapper[0].height || $wrapper.height();
    var wrapperWidth = $wrapper[0].width || $wrapper.width();
    
    // get our original content dimensions
    dimension.width = $content[0].width || $content.width();
    dimension.height = $content[0].height || $content.height();
    dimension.aspect = dimension.width / dimension.height;
    
    // scale height
    dimension.scale = wrapperHeight / dimension.height;
    dimension.height = wrapperHeight;
    dimension.width *= dimension.scale;
    
    // scale width
    if (dimension.width > wrapperWidth) {
        dimension.scale = wrapperWidth / dimension.width;
        dimension.width = wrapperWidth;
        dimension.height *= dimension.scale;
    }
    
    // update content object
    $content.width(dimension.width);
    $content.height(dimension.height);
}

var ShowModal = function (html, height, width) {
    
    $content = $("#modalContent");
    $wrapper = $("#modalWrapper");

    $content.html(html);
    ScaleToFit($content, $wrapper);
    
    if (height != undefined) {
        $("#modalContent").height(height);
    }
    if (width != undefined) {
        $("#modalContent").width(width);
    }
    
    $("#modalWrapper").fadeIn(300);
}

var UpdateProgressBar = function (percent) {
    var progressBarWidth = percent * $("#progressBar").width() / 100;
    $("#progressBar").find('div').animate({ width: progressBarWidth }, 10).html(percent + "%&nbsp;");
}

/************************************************************************
    ON READY
**************************************************************************/

$(document).ready(function() {
    $(document).on("click", "img", function () {
        if ($(this).data("lightbox") != undefined) {

            // scale used to leave padding around full screen image
            var imgScale = 0.9;

            // get our modal html items
            $modalContent = $("#modalContent");
            $modalWrapper = $("#modalWrapper");

            // create new html img
            var img = new Image();
            var $img = $(img);
            var src = $(this).attr("src");

            // set image src and size
            $img.attr('src', src);
            $img.height($(this).height());
            $img.width($(this).width());
            
            // scale the image so it will be full screen
            ScaleToFit($img, $modalWrapper);

            // add padding around image too make room for description
            $img.width($img.width() * imgScale);
            $img.height($img.height() * imgScale);

            // and img to modal and show modal
            $modalContent.html(img);
            $modalWrapper.fadeIn(300);
        }
    });
    
    $("#modalWrapper").click(function () {
        $("#modalWrapper").fadeOut(300);
    }); 
});